<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Posts</title>
	<?php require "header.php" ?>
	<script src="//cdn.ckeditor.com/4.7.0/basic/ckeditor.js"></script>
</head>
<body>
	<div class="container">
		<div class="page-header">
			<h1>Admin <a class="btn btn-xs btn-primary" href="./index.php">main</a></h1>
		</div>
		<section class="row" id="adminApp">
			<div role="tabpanel">
				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active">
						<a href="#add" aria-controls="add" role="tab" data-toggle="tab" v-on:click="resetPostEditor()">
							<span v-if="post.aId != -1">Modifica</span>
							<span v-else>Adauga</span>
						</a>
					</li>
					<li role="presentation">
						<a v-on:click="resetPostEditor(); getArticles()" href="#list" aria-controls="list" role="tab" data-toggle="tab">List</a>
					</li>
				</ul>
			
				<!-- Tab panes -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="add">
						<div class="well">
							<legend v-if="post.aId != -1">Modifica</legend>
							<legend v-else>Adauga</legend>
						
							<div class="form-group">
								<label for="">Titlu</label>
								<input type="text" class="form-control" v-model="post.title" id="" placeholder="Titlu">
								<p v-if="errors.invalidTitle" class="text-danger">Completati acest camp</p>
							</div>
							<div class="form-group">
								<label for="">Continut</label>
								<textarea id="contentEditor" ></textarea>
								<p v-if="errors.invalidContent" class="text-danger">Completati acest camp</p>
							</div>
							
						
							<button class="btn btn-primary" v-on:click="saveArticle">Submit</button>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="list">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>ID</th>
									<th>Title</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<tr v-for="p in posts">
									<td>{{p.aId}}</td>
									<td>{{p.title}}</td>
									<td>
										<button class="btn btn-xs btn-primary" v-on:click="editArticle(p)">Edit</button>
										<button class="btn btn-xs btn-danger" v-on:click="deleteArticle(p.aId)">Delete</button>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</section>
	</div>
	<script src="./assets/admin.js"></script>
</body>
</html>