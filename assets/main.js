var articles = new Vue({
	el: "#articles",
	data: {
		posts: []
	},
	created: function() {
		this.getArticles();
	},
	methods: {
		getArticles: function() {
			var _this = this;
			axios.post("./api.php?action=getArticles").then(function(result) {
				_this.posts = result.data;
			})
		},
	}
});