<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Posts</title>
	<?php require "header.php" ?>
	<link rel="stylesheet" href="./assets/style.css">
</head>
<body>
	<div class="container" id="articles">
		<div class="page-header">
			<h1>Articole <a class="btn btn-xs btn-primary" href="./admin.php">admin</a></h1>
		</div>
		<section class="row articles">
			<article class="article" v-for="p in posts">
				<h2 class="article-title">{{p.title}}</h2>
				<div class="content well" v-html="p.content">
				</div>
			</article>
		</section>
	</div>
	<script src="./assets/main.js"></script>
</body>
</html>