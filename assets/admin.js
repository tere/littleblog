$(document).ready(function() {
	
	var adminApp = new Vue({
		el: "#adminApp",
		data: {
			posts: [],
			post: {
				aId: -1,
				title: "",
				content: "",
			},
			errors: {
				invalidTitle: false,
				invalidContent: false
			}
		},
		created: function() {
			this.getArticles();
		},
		methods: {
			getArticles: function() {
				var _this = this;
				axios.post("./api.php?action=getArticles").then(function(result) {
					_this.posts = result.data;
				})
			},
			saveArticle: function() {
				this.errors.invalidTitle = false;
				this.errors.invalidContent = false;
			 	this.post.content = CKEDITOR.instances.contentEditor.getData();
				if (this.post.title.length == 0) {
					this.errors.invalidTitle = true;
					if(this.post.content.length == 0) {
						this.errors.invalidContent = true;
						return;
					}
					return;
				}
				var _this = this;
				axios.post("./api.php?action=saveArticle", {post: this.post}).then(function(result) {
					alert(result.data.message);
					_this.getArticles();
				})
			},
			editArticle: function(post) {
				this.post = post;
				CKEDITOR.on('instanceReady', function() { CKEDITOR.instances.contentEditor.setData(post.content); } );
				jQuery('.nav-tabs a[href="#add"').tab('show');
			},
			deleteArticle: function(postId) {
				var _this = this;
				axios.post("./api.php?action=deleteArticle", {postId: postId}).then(function(result) {
					_this.getArticles();
				})
			},
			resetPostEditor: function() {
				this.post = {
					aId: -1,
					title: "",
					content: "",
				};
				CKEDITOR.on('instanceReady', function() { CKEDITOR.instances.contentEditor.setData(post.content); } );
			}
		}
	})
	var editor = CKEDITOR.replace('contentEditor', { height: 200 });
})