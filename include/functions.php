<?php
require __DIR__  . "/db.php";

function saveArticle($post) {
	global $db;
	$r = array(
		"type" => 0,
		"message" => "Ups"
	);
	if($post->aId  == -1) {
		//new 
		$title = $db->escape_string($post->title);
		$content = $db->escape_string($post->content);
		$query = "INSERT INTO `articol` (`title`, `content`) 
					VALUES ('{$title}', '{$content}')";
		$result = $db->query($query);
		if($result) {
			$r['type'] = 1;
			$r['message'] = "Adaugat";
		} else {
			$r['message'] = "Eroare la adaugare";
		}
	} else if (isset($post->aId)) {
		//existing
		$id = $db->escape_string($post->aId);
		$title = $db->escape_string($post->title);
		$content = $db->escape_string($post->content);
		$query = "UPDATE `articol`
					SET `title` = '{$title}',
						`content` = '{$content}'
					WHERE `aId` = {$id}";
		$result = $db->query($query);
		if($result) {
			$r['type'] = 1;
			$r['message'] = "Actualizat";
		} else {
			$r['message'] = "Eroare la actualizare";
		}
	}
	echo json_encode($r);
}

function getArticles() {
	global $db;
	$r = array();

	$query = "SELECT * FROM `articol`";
	$result = $db->query($query);
	if($result && $result->num_rows) {
		while($row = $result->fetch_assoc()){
			array_push($r, $row);
		}
	}
	echo json_encode($r);
}

function deleteArticle($id) {
	global $db;
	$r = array();
	$query = "DELETE FROM `articol` WHERE `aId` = {$id}";
	
	if($db->query($query)) {
		$r['type'] = 1;
	} else {
		$r['type'] = 0;
	}

	echo json_encode($r);
}