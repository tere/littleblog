<?php


require __DIR__ . "/include/functions.php";
$post = json_decode(file_get_contents("php://input"));
switch ($_GET['action']) {
	case 'saveArticle':
		saveArticle($post->post);
		break;
	case 'deleteArticle':
		deleteArticle($post->postId);
		break;
	case 'getArticles':
		getArticles();
		break;
}